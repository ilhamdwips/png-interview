package com.example.pgninterview.customers;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
public class CustomerService  {

    private final CustomerRepository customerRepository;

    public CustomerService(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    public ResponseEntity<Map<String, Object>> getCustomers(int page){
        List<Customer> customerData = new ArrayList<Customer>();
        Pageable paging = PageRequest.of(page, 10);
        Page<Customer> pageData = customerRepository.findAll(paging);

        customerData = pageData.getContent();

        Map<String, Object> response = new HashMap<>();
        response.put("customers", customerData);
        response.put("currentPage", pageData.getNumber());
        response.put("totalItems", pageData.getTotalElements());
        response.put("totalPages", pageData.getTotalPages());

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    public Customer addNewCustomer(Customer customer) {
        Customer customerData = customerRepository.save(customer);
        return customerData;
    }
}
