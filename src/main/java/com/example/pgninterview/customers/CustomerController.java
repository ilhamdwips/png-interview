package com.example.pgninterview.customers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(path = "/customer")
public class CustomerController {

    private final CustomerService customerService;

    @Autowired
    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping
    public ResponseEntity<Map<String, Object>> getCustomer(
            @RequestParam(defaultValue = "0") int page
    ){
        return customerService.getCustomers(page);
    }

    @PostMapping
    public Customer addNewCustomer(@RequestBody Customer customer){
        return customerService.addNewCustomer(customer);
    }
}
