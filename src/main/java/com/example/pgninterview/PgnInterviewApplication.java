package com.example.pgninterview;

import com.example.pgninterview.customers.Customer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@SpringBootApplication
public class PgnInterviewApplication {

    public static void main(String[] args) {
        SpringApplication.run(PgnInterviewApplication.class, args);
    }
}
